#include <stdio.h>



void llenarMatriz(int matriz[9][9]) {
    int num = 1;
    for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            matriz[i][j] = num++;
        }
    }
}

void imprimirMatriz(int matriz[9][9]) {
    for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            printf("%d\t", matriz[i][j]);
        }
        printf("\n");
    }
}


void buscarNumero(int matriz[9][9], int num) {
    int encontrado = 0; 
    for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            if (matriz[i][j] == num) {
                printf("El número %d está en la posicion [%d][%d]\n", num, i, j);
                encontrado = 1;
            }
        }
    }
    if (!encontrado) {
        printf("El número %d no se encuentra en la matriz\n", num);
    }
}

int main() {
    int matriz[9][9];
    int num_buscar;

    
    llenarMatriz(matriz);

    
    printf("Matriz generada:\n");
    imprimirMatriz(matriz);

    
    printf("\nIngrese un número para buscar en la matriz: ");
    scanf("%d", &num_buscar);

    
    buscarNumero(matriz, num_buscar);

    return 0;
}
